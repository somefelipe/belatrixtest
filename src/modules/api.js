import axios from 'axios';

/**
 * Future version will add symbols if is needed, otherwise should not include it
 * in order to get full rates for the current base
 */
const getConversionRate = (base, target) => {
    return axios.get(`http://api.fixer.io/latest?base=${base}&symbols=${target}`)
}

export default {
    getConversionRate
}