const defaultBase     = "USD",
      defaultTarget   = "EUR",
      defaultRate     = 1,
      requestInterval = 10000;

export default {
    defaultBase,
    defaultTarget,
    defaultRate,
    requestInterval
}