import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../logo.svg';
import '../../App.css';

const brand = "MoneyXchange"

function Header(prop){
    const props  = prop || {},
          base   = props.base || "",
          target = props.target || "",
          pageHeading = `Converter from ${base} to ${target}`;

    return (
        <header className="App__header">
            <img src={logo} className="App__logo" alt="logo" />
            <h1 className="App__title">{brand}</h1>
            <h2 className="App__title">{pageHeading}</h2>
        </header>
    );
}

Header.propTypes = {
    base   : PropTypes.string,
    target : PropTypes.string
}

export default Header;