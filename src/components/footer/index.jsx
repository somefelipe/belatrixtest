import React from 'react';
import '../../App.css';

const brand = "Thanks for visiting us!"

function Footer(){
    return (
        <footer className="App__footer">
            <h1 className="App__footer-message">{brand}</h1>
        </footer>
    );
}

Footer.propTypes = {
}

export default Footer;