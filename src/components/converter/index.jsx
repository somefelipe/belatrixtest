import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Button, Container, Segment } from 'semantic-ui-react'
import '../../App.css';

const EUR_FORMAT   = {style: "currency", currency: "EUR"}, //TODO update this format to handle other currency
      INPUT_FORMAT = { style: "currency", currency: "USD", maximumFractionDigits: 4 },
      BUTTON_TEXT  = "¡Xchange!",
      INPUT_MESSAGE = "How much?";


class Converter extends Component {

    getNumericValue(currency){
        return parseFloat(currency.replace(/[^\d.]/g, ''));
    }

    calculate(input, target, rate){
        const value           = input.inputRef.value,
            calculatedValue = value*rate,
            formattedValue  = new Intl.NumberFormat("de-DE", EUR_FORMAT).format(calculatedValue);

        if(!isNaN(calculatedValue)){
            target.inputRef.value = (formattedValue);
        }else{
            target.inputRef.value = '';
        }
    }

    trim(e, input, display, target){
        var newValue = e.target.value,
            cleanUpValue = newValue.replace(/[^\d.]/g, ''),
            formattedValue = new Intl.NumberFormat('en-US', INPUT_FORMAT).format(cleanUpValue);

        display.inputRef.value = formattedValue;
        target.inputRef.value = '';
    }

    render(){
        const props  = this.props || {},
              base   = props.base,
              target = props.target,
              rate   = props.rate;

        let baseInput,
            displayInput,
            targetInput;

        return (
            <Container className={"App__converter"}>
                <Segment className={"App__input-container"}>
                    <Input  className={"App__input"}
                            placeholder={INPUT_MESSAGE}
                            ref ={(element)=> baseInput=element}
                            onChange={(e)=>this.trim(e, baseInput, displayInput, targetInput)}/>

                    <Input  className={"App__input-disabled"}
                            placeholder={base}
                            label = "Origin"
                            ref ={(element)=> displayInput=element}
                            disabled />

                    <Input  className={"App__input-disabled"}
                            placeholder={target}
                            label = "Xchanged"
                            ref={(element)=> targetInput=element}
                            disabled />
                </Segment>

                <Button className={"App__converter-button"}
                        onClick={()=>(this.calculate(baseInput, targetInput, rate))}>
                    {BUTTON_TEXT}
                </Button>
            </Container>
        );
    }
}

Converter.propTypes = {
    base   : PropTypes.string,
    target : PropTypes.string,
    rate   : PropTypes.number
}

export default Converter;