import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import {expect} from 'chai';
import Adapter from 'enzyme-adapter-react-16';
import Converter from './';

Enzyme.configure({ adapter: new Adapter() });

describe("Converter component:", ()=>{
    it("Should calculate properly the exchanged based on a given rate", ()=>{
        const converter = shallow(<Converter />),
              input     = {inputRef:{value: "1"}},
              rate      = 0.5,
              target    = {inputRef:{value: ""}},
              expected  = "€ 0.50";

        converter.instance().calculate(input, target, rate);

        expect(target.inputRef.value).to.equal(expected);
    });
});