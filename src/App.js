import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';
import Header from './components/header';
import Footer from './components/footer';
import Converter from './components/converter';
import config from './config';
import API from './modules/api'
import './App.css';

class App extends Component {

    constructor(){
        super();

        //these values can be updated from everywhere to let the app do other xchanges
        this.base   = config.defaultBase;
        this.target = config.defaultTarget;
        this.requestInterval = config.requestInterval;

        this.ratesRequest();
        setInterval(this.ratesRequest.bind(this), this.requestInterval);
    }

    ratesRequest(){
        API.getConversionRate(this.base, this.target).then(this.ratesRequestHandler.bind(this));
    }

    ratesRequestHandler(data){
        const state        = this.state || {},
              currentBase  = state.base || this.base,
              currentTarget= state.target || this.target,
              response     = data || {},
              responseData = response.data || {},
              rates        = responseData.rates || {},
              rate         = rates[currentTarget] || config.defaultRate;

        this.setState({rate: rate, base: currentBase, target: currentTarget});
    }

    render() {
        const state  = this.state   || {},
              base   = state.base   || config.defaultBase,
              target = state.target || config.defaultTarget,
              rate   = state.rate   || config.defaultRate;

        return (
            <Container className="App">
                <Header base  = {base}
                        target= {target} />

                <Converter base   = {base}
                           target = {target}
                           rate   = {rate} />
                <Footer />
            </Container>
        );
    }
}

export default App;
